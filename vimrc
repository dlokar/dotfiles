set nocompatible
syntax enable
filetype plugin indent on

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Bundle 'altercation/vim-colors-solarized'
Plugin 'scrooloose/nerdTree'
call vundle#end()

set backupcopy=yes

set swapfile
set dir=~/tmp

let mapleader = "\<Space>"

set encoding=utf-8

set path+=**
set wildmenu

set tabstop=4
set shiftwidth=4
set expandtab
set cindent

set autoread
set autowrite
set autowriteall
:au FocusLost * :wa

set colorcolumn=100

set nu
set relativenumber

set incsearch
set complete -=i

set hidden

set laststatus=2

setlocal spell spelllang=en_us

set hlsearch

set background=dark
set t_Co=256
colorscheme solarized

if executable('ag')
    set grepprg=ag\ --vimgrep\ --ignore-dir=cache\ --ignore-dir=web/js\ --ignore-dir=vendor\ --ignore-dir=build\ --ignore=tags\ $*
    set grepformat=%f:%l:%c:%m
endif

set ruler

nmap <c-n> :NERDTreeToggle<cr>
nmap <leader>r :NERDTreeFind<cr>
let NERDTreeIgnore=['^__pycache__$', '^__init__.py$']

let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

"YAML syntax is slow
autocmd BufNewFile,BufRead *.yml  set syntax=no_syntax

if &diff
    set norelativenumber
    nmap <leader>q :qa<cr>
    set diffopt+=iwhite
    syntax off
endif

